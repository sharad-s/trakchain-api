const multer = require("multer");
const path = require("path");
const isEmpty = require("../utils/isEmpty");
const storage = multer.memoryStorage();

const maxSize = 30 * 1000 * 1000; //30 MB

/*
Profile Upload
*/
const upload = multer({ limits: { fileSize: maxSize } });
// upload of track  with fields 'fileAudio': audio and 'fileImage': image
const trackUpload = upload.fields([
  { name: "fileImage", maxCount: 1 },
  { name: "fileAudio", maxCount: 1 }
]);

function validateTrackUpload(req, res, next) {
  console.log("validateTrackUpload middleware...");

  const files = req.files;
  const errors = {};
  console.log(isEmpty(errors), ": is Empty? ");
  if (!files.fileImage)
    return res.status(404).json({ noImage: "Please Upload 'fileImage'" });
  if (!files.fileAudio)
    return res.status(404).json({ noAudio: "Please Upload 'fileAudio'" });
  // if (files.fileImage[0].mimetype.split('/')[0] !== 'image')
  //   return res.status(400).send("Please upload an image for 'fileImage'.");
  // if (files.fileAudio[0].mimetype.split('/')[0] !== 'audio')
  //   return res.status(400).send("Please upload an audio file for 'fileAudio'.");
  next();
}

/*
Profile Upload
*/
// upload of track  with fields 'coverPhoto': audio and 'profileImage': image
const profileUpload = upload.fields([
  { name: "coverPhoto", maxCount: 1 },
  { name: "profileImage", maxCount: 1 }
]);

function validateProfileUpload(req, res, next) {
  const files = req.files;
  if (files) {
    if (
      files.coverPhoto &&
      files.coverPhoto[0].mimetype.split("/")[0] !== "image"
    )
      return res
        .status(404)
        .json({ noCoverPhoto: "Please Upload an image for 'coverPhoto'" });
    if (
      files.profileImage &&
      files.profileImage[0].mimetype.split("/")[0] !== "image"
    )
      return res
        .status(404)
        .json({ noProfileImage: "Please Upload  an image for 'profileImage'" });
  }
  next();
}

module.exports = {
  trackUpload,
  validateTrackUpload,
  profileUpload,
  validateProfileUpload
};
