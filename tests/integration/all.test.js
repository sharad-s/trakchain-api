const request = require('supertest');
const { Tag } = require('../../models/tag');

let server;

// TAGS.test.js
describe('/api/tags', () => {
  beforeEach(async () => {
    // Run the express server
    server = require('../../index');
  });

  afterEach(async () => {
    // Close server and clear testing database
    await server.close();
    await Tag.remove({});
  });

  describe('GET /', () => {
    it('should return all tags', async () => {
      await Tag.collection.insertMany([
        { name: 'tag1' },
        { name: 'tag2' },
        { name: 'tag3' }
      ]);

      const res = await request(server).get('/api/tags');
      expect(res.status).toBe(200);
      expect(res.body.length).toBe(3);
      expect(res.body.some(t => t.name === 'tag1')).toBeTruthy();
      expect(res.body.some(t => t.name === 'tag2')).toBeTruthy();
      expect(res.body.some(t => t.name === 'tag3')).toBeTruthy();
    });
  });
  /*
  describe("GET /:id", async () => {
    it("should return a tag if a valid id is passed", async () => {
      const tag = new Tag({ name: "tag1" });
      await tag.save();

      const res = await request(server).get("/api/tags/" + tag._id);
      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("name", tag.name); //expect name to have tag.name
    });

    it("should return 404 if an invalid id is passed", async () => {
      const res = await request(server).get("/api/tags/2");
      expect(res.status).toBe(404);
    });
  });
*/
  describe('POST /', async () => {
    // let token;
    let name;

    // Standard code for making a POST request
    const exec = async () => {
      return await request(server)
        .post('/api/tags/')
        .send({ name });
    };

    // Happy Path
    beforeEach(() => {
      // token = new User().generateAuthToken();
      name = 'tag1';
    });

    it('should return 400 if tag is less than 3 characters', async () => {
      name = '12';
      const res = await exec();
      expect(res.status).toBe(400);
    });

    it('should return 400 if tag is more than 15 characters', async () => {
      name = new Array(20).join('a');
      const res = await exec();
      expect(res.status).toBe(400);
    });

    // Happy Path
    it('should save the tag if it is valid', async () => {
      await exec();
      const tag = await Tag.find({ name: 'tag1' });
      expect(tag).not.toBeNull();
    });

    // Happy Path
    it('should return the tag in the response if valid', async () => {
      const res = await exec();
      expect(res.body).toHaveProperty('_id');
      expect(res.body).toHaveProperty('name', 'tag1');
    });
  });
});

/*********************
 *                     *
 *       TRACKS        *
 *                     *
 **********************/

// TRACKS.test.js
// const request = require("supertest");
// const { Tag } = require("../../models/tag");
const { Track } = require('../../models/track');
const { User } = require('../../models/user');

// let server;

describe('/api/tracks', () => {
  beforeEach(async () => {
    server = require('../../index');
  });

  afterEach(async () => {
    await server.close();
    await Tag.remove({});
    await Track.remove({});
  });

  describe('GET /', () => {
    it('should return all tracks', async () => {
      server = require('../../index');

      await Track.collection.insertMany([
        {
          name: 'Track01',
          ipfsHash: 'IPFS_HASH01',
          imageHash: 'IMAGE_HASH01',
          tags: ['tag01']
        },
        {
          name: 'Track02',
          ipfsHash: 'IPFS_HASH02',
          imageHash: 'IMAGE_HASH02',
          tags: ['tag02']
        },
        {
          name: 'Track03',
          ipfsHash: 'IPFS_HASH03',
          imageHash: 'IMAGE_HASH03',
          tags: ['tag03']
        }
      ]);

      const res = await request(server).get('/api/tracks');
      expect(res.status).toBe(200);
      expect(res.body.length).toBe(3);
      expect(res.body.some(t => t.name === 'Track01')).toBeTruthy();
      expect(res.body.some(t => t.name === 'Track02')).toBeTruthy();
      expect(res.body.some(t => t.name === 'Track03')).toBeTruthy();
      await server.close();
      await Tag.remove({});
      await Track.remove({});
    });
  });

  describe('GET /:id', async () => {
    it('should return a track if a valid id is passed', async () => {
      // Create a new tag object
      const tag1 = new Tag({ name: 'TAG01' });
      await tag1.save;
      const tag2 = new Tag({ name: 'TAG02' });
      await tag2.save;

      // Create new track object
      const track = new Track({
        name: 'TRACK01',
        ipfsHash: 'IPFS_HASH01',
        imageHash: 'IPFS_HASH02',
        tags: [tag1, tag2]
      });
      await track.save();

      // Get track by id
      const res = await request(server).get('/api/tracks/' + track._id);
      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty('name', track.name); //expect name to have tag.name
    });

    it('should return 404 if an invalid id is passed', async () => {
      const res = await request(server).get('/api/tracks/2');
      expect(res.status).toBe(404);
    });
  });

  describe('GET /tag/:tagName', async () => {
    it('should return all proper matching tracks if a valid tag is passed', async () => {
      // Create a new tag object
      const tag1 = new Tag({ name: 'CARTI' });
      await tag1.save;
      const tag2 = new Tag({ name: 'UZI LONDON' });
      await tag2.save;
      const tag3 = new Tag({ name: 'THUGGER' });
      await tag3.save;

      // Create new track object
      const track1 = new Track({
        name: 'GONE',
        ipfsHash: 'IPFS_HASH01',
        imageHash: 'IPFS_HASH02',
        tags: [tag1, tag3]
      });
      await track1.save();
      // Create new track object
      const track2 = new Track({
        name: 'POOLS',
        ipfsHash: 'IPFS_HASH01',
        imageHash: 'IPFS_HASH02',
        tags: [tag1, tag2]
      });
      await track2.save();
      // Create new track object
      const track3 = new Track({
        name: 'HONGKONG',
        ipfsHash: 'IPFS_HASH01',
        imageHash: 'IPFS_HASH02',
        tags: [tag2, tag3]
      });
      await track3.save();

      // Get track by tags
      const res = await request(server).get('/api/tracks/tag/' + tag2.name);
      expect(res.status).toBe(200);
      expect(res.body.length).toBe(2); //expect name to have tag.name
    });

    it('should return 404 if an invalid tag is passed', async () => {
      const res = await request(server).get('/api/tracks/tag/' + 'POOPOO');
      expect(res.status).toBe(404);
    });
  });

  describe('POST /', async () => {
    let token;
    let name, ownerId, ipfsHash, imageHash, tags;

    // Standard code for making a POST request
    const exec = async () => {
      return await request(server)
        .post('/api/tracks/')
        .set('x-auth-token', token)
        .send({ name, ipfsHash, imageHash, tags });
    };

    // Happy Path
    beforeEach(() => {
      token = new User().generateAuthToken();
      name = 'TRACK01';
      ipfsHash = 'IPFS_HASH01';
      imageHash = 'IMAGE_HASH01';
      tags = ['tag01', 'tag02'];
    });

    it('should return 400 if track name is empty', async () => {
      name = '';
      const res = await exec();
      expect(res.status).toBe(400);
    });

    it('should return 400 if track name is more than 30 characters', async () => {
      name = new Array(32).join('a');
      const res = await exec();
      expect(res.status).toBe(400);
    });

    it('should return 400 if track ipfs hash is empty', async () => {
      ipfsHash = '';
      const res = await exec();
      expect(res.status).toBe(400);
    });

    // it("should return 400 if track ipfs hash is not a valid ipfs hash", async () => {
    //   imageHash = "";
    //   const res = await exec();
    //   expect(res.status).toBe(400);
    // });

    it('should return 400 if track imageHash is empty', async () => {
      imageHash = '';
      const res = await exec();
      expect(res.status).toBe(400);
    });

    it('should return 400 if no tags are uploaded', async () => {
      tags = undefined;
      const res = await exec();
      expect(res.status).toBe(400);
    });

    it('should return 400 if tags are uploaded as not a string array', async () => {
      tags = [1, 2, 3];
      let res = await exec();
      expect(res.status).toBe(400);
      tags = [{ x: 3 }, { y: 3 }];
      res = await exec();
      expect(res.status).toBe(400);
    });

    // Happy Path
    it('should save the track if it is valid', async () => {
      const res = await exec();
      console.log(res.body);
      const track = await Track.find({ name: 'TRACK01' });
      expect(res.status).toBe(200);
      expect(track).not.toBeNull();
    });

    // Happy Path
    it('should return the track in the response if valid', async () => {
      const res = await exec();
      console.log(res);
      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty('_id');
      expect(res.body).toHaveProperty('name', 'TRACK01');
    });

    // Happy Path
    it('should properly save multiple tracks with overlapping tags', async () => {
      // Track01
      let res = await exec();
      expect(res.status).toBe(200);
      let track = await Track.find({ name: 'TRACK01' });
      expect(track).not.toBeNull();

      // Track02
      name = 'TRACK02';
      ipfsHash = 'IPFS_HASH02';
      imageHash = 'IMAGE_HASH02';
      tags = ['tag01', 'tag02', 'tag03', 'tag04'];
      res = await exec();
      expect(res.status).toBe(200);
      track = await Track.find({ name: 'TRACK02' });
      expect(track).not.toBeNull();
    });
  });
});

/*********************
 *                     *
 *       USERS         *
 *                     *
 **********************/

// users.test.js
// const request = require("supertest");
// const { Tag } = require("../../models/tag");
// const { Track } = require("../../models/track");
// const { User } = require('../../models/user');

describe('/api/users', () => {
  beforeEach(async () => {
    // Run the express server
    server = require('../../index');
  });

  afterEach(async () => {
    // Close server and clear testing database
    await server.close();
    await User.remove({});
  });

  describe('GET /', () => {
    it('should return all users', async () => {
      await User.collection.insertMany([
        {
          name: 'username01',
          email: 'email01@gmail.com',
          password: 'password01',
          firstName: 'User',
          lastName: 'Name'
        },
        {
          name: 'username02',
          email: 'email02@gmail.com',
          password: 'password02',
          firstName: 'User',
          lastName: 'Name'
        },
        {
          name: 'username03',
          email: 'email03@gmail.com',
          password: 'password03',
          firstName: 'User',
          lastName: 'Name'
        }
      ]);
      //
      const res = await request(server).get('/api/users');
      expect(res.status).toBe(200);
      expect(res.body.length).toBe(3);
      // expect(res.body.some(t => t.name === "tag1")).toBeTruthy();
      // expect(res.body.some(t => t.name === "tag2")).toBeTruthy();
      // expect(res.body.some(t => t.name === "tag3")).toBeTruthy();
    });
  });
});
