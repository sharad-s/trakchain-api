const request = require("supertest");
const { Tag } = require("../../models/tag");

let server;

describe("/api/tags", () => {
  beforeEach(async () => {
    // Run the express server
    server = require("../../index");
  });

  afterEach(async () => {
    // Close server and clear testing database
    await server.close();
    await Tag.remove({});
  });

  describe("GET /", () => {
    it("should return all tags", async () => {
      await Tag.collection.insertMany([
        { name: "tag1" },
        { name: "tag2" },
        { name: "tag3" }
      ]);

      const res = await request(server).get("/api/tags");
      console.log(res.body);
      expect(res.status).toBe(200);
      expect(res.body.length).toBe(3);
      expect(res.body.some(t => t.name === "tag1")).toBeTruthy();
      expect(res.body.some(t => t.name === "tag2")).toBeTruthy();
      expect(res.body.some(t => t.name === "tag3")).toBeTruthy();
    });
  });
  /*
  describe("GET /:id", async () => {
    it("should return a tag if a valid id is passed", async () => {
      const tag = new Tag({ name: "tag1" });
      await tag.save();

      const res = await request(server).get("/api/tags/" + tag._id);
      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("name", tag.name); //expect name to have tag.name
    });

    it("should return 404 if an invalid id is passed", async () => {
      const res = await request(server).get("/api/tags/2");
      expect(res.status).toBe(404);
    });
  });
*/
  describe("POST /", async () => {
    // let token;
    let name;

    // Standard code for making a POST request
    const exec = async () => {
      return await request(server)
        .post("/api/tags/")
        .send({ name });
    };

    // Happy Path
    beforeEach(() => {
      // token = new User().generateAuthToken();
      name = "tag1";
    });

    it("should return 400 if tag is less than 3 characters", async () => {
      name = "12";
      const res = await exec();
      expect(res.status).toBe(400);
    });

    it("should return 400 if tag is more than 15 characters", async () => {
      name = new Array(20).join("a");
      const res = await exec();
      expect(res.status).toBe(400);
    });

    // Happy Path
    it("should save the tag if it is valid", async () => {
      await exec();
      const tag = await Tag.find({ name: "tag1" });
      expect(tag).not.toBeNull();
    });

    // Happy Path
    it("should return the tag in the response if valid", async () => {
      const res = await exec();
      expect(res.body).toHaveProperty("_id");
      expect(res.body).toHaveProperty("name", "tag1");
    });
  });
});
