/*
  Route: /api/rentals

*/

const { Transaction, validateTransaction } = require("../models/transaction");
const { Track } = require("../models/track");
const { User } = require("../models/user");
const { Tag } = require("../models/tag");

const express = require("express");
const mongoose = require("mongoose");
const Fawn = require("fawn");
const router = express.Router();

// Error Transformer for client
const errorTransformer = require("../utils/errorTransformer");

// Middlware
const validateObjectId = require("../middleware/validateObjectId");
const passportAuth = require("../middleware/passportAuth");

// GET all genres
router.get("/", async (req, res) => {
  const transactions = await Transaction.find();
  res.send(transactions);
});

// GET tx by id
router.get("/:id", validateObjectId, async (req, res) => {
  // Find genre by _id
  const tx = await Transaction.findById(req.params.id);
  if (!tx) {
    return res
      .status(404)
      .json({ notfound: "The transaction with the given ID was not found." });
  }
  res.send(tx);
});

// GET txs logged in user sold
router.get("/me/sold", passportAuth, async (req, res) => {
  // Find genre by _id
  const txs = await Transaction.find({ "vendor._id": req.user._id });
  if (!txs) {
    return res
      .status(404)
      .json({ notfound: "The transactions for this user was not found." });
  }
  res.send(txs);
});

// GET txs logged in user bought
router.get("/me/bought", passportAuth, async (req, res) => {
  // Find genre by _id
  const txs = await Transaction.find({ "customer._id": req.user._id });
  if (!txs) {
    return res
      .status(404)
      .json({
        notfound: "The transactions for this <us></us>er was not found."
      });
  }
  res.send(txs);
});

// POST new transaction
router.post("/", async (req, res) => {
  // Validate rental input. If validation fails, return 400 - Bad Request
  const { error } = validateTransaction(req.body);
  if (error) return res.status(400).json(errorTransformer(error));

  // Validate the Rental's customer property. If customer does not exist, return 400 - Bad Request
  const customer = await User.findById(req.body.customerId);
  if (!customer) return res.status(400).json({ customer: "Invalid Customer" });

  // Validate the Rental's customer property. If customer does not exist, return 400 - Bad Request
  const vendor = await User.findById(req.body.vendorId);
  if (!vendor) return res.status(400).json({ vendor: "Invalid Vendor" });

  // Validate the Rental's movie property. If movie does not exist, return 400 - Bad Request
  const track = await Track.findById(req.body.trackId);
  if (!track) return res.status(400).json({ track: "Invalid Track" });

  // Validate same transaction doesn't already exist
  let foundTransaction = await Transaction.findOne({
    "vendor._id": req.body.vendorId,
    "customer._id": req.body.customerId,
    "track._id": req.body.trackId
  });
  if (foundTransaction) {
    console.log("foundTransaction", foundTransaction);
    return res.status(400).json({ transaction: "Transaction already exists" });
  }

  // Validate that track belongs to vendor
  if (track.owner._id.toString() !== vendor._id.toString()) {
    console.log("Track owner:", track.owner._id, vendor._id);
    return res
      .status(400)
      .json({ transaction: "Track does not belong to vendor" });
  }

  // Create new movie object
  // Selectively choose which properties of the client object to add to the database
  // Hybrid Approach to referencing genre
  let transaction = new Transaction({
    vendor: {
      _id: vendor._id,
      username: vendor.username,
      email: vendor.email,
      firstName: vendor.firstName,
      lastName: vendor.lastName,
      role: "PRODUCER"
    },
    customer: {
      _id: customer._id,
      username: customer.username,
      email: customer.email,
      firstName: customer.firstName,
      lastName: customer.lastName,
      role: "CUSTOMER"
    },
    track: {
      _id: track._id,
      name: track.name,
      owner: track.owner,
      ipfsHash: track.ipfsHash,
      imageHash: track.imageHash,
      price: track.price,
      tags: track.tags
    },
    price: {
      type: Number,
      default: track.price
    }
  });

  // Fawn takes in a collection named in the db, then the object to be saved
  // Fawn is good for two-phase commits to the DB
  try {
    new Fawn.Task().save("transactions", transaction).run();

    res.send(transaction);
  } catch (ex) {
    res.status(500).send("Something failed.");
  }
});

module.exports = router;
