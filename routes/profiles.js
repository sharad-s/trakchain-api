const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const ipfs = require("../utils/ipfs");

// Models
const { Profile, validateProfileInput } = require("../models/profile");
const { User } = require("../models/user");

// Middleware
const passportAuth = require("../middleware/passportAuth");
const {
  profileUpload,
  validateProfileUpload
} = require("../middleware/multiPartForms");
// @route     GET api/profiles/test
// @desc      Tests profile route
// @access    Public
router.get("/test", (req, res) => res.json({ message: "Profile works" }));

// @route     GET api/profiles
// @desc      Gets current User's profile
// @access    Private
router.get("/", async (req, res) => {
  const errors = {};

  const profiles = await Profile.find();

  if (!profiles) {
    errors.noprofile = "Could not find profiles";
    return res.status(404).json(errors);
  }
  res.json(profiles);
});

// @route     GET api/profiles/me
// @desc      Gets current User's profile
// @access    Private
router.get("/me", passportAuth, async (req, res) => {
  const errors = {};

  const profile = await Profile.findOne({
    "user._id": req.user._id
  });

  if (!profile) {
    errors.noprofile = "There is No Profile for this user";
    return res.status(404).json(errors);
  }
  res.json(profile);
});

// @route     POST api/profile
// @desc      Create or Edit current User's profile
// @access    Private
router.post(
  "/",
  passportAuth,
  profileUpload,
  validateProfileUpload,
  async (req, res) => {
    // Extract coverPhoto, profileImage from req.files
    if (req.files) {
      const { coverPhoto, profileImage } = req.files;

      // Upload files to IPFS and populate req.body
      // Upload coverphoto
      if (coverPhoto) {
        console.log("i got a cover phtoo");
        const coverPhotoUpload = {
          path: coverPhoto[0].originalname,
          content: coverPhoto[0].buffer
        };
        const coverPhotoIPFSHash = await ipfsAddImage(coverPhotoUpload);
        req.body.coverPhotoIPFSHash = coverPhotoIPFSHash[0].hash;
      }
      // Upload profileImage
      if (profileImage) {
        profileImageUpload = {
          path: profileImage[0].originalname,
          content: profileImage[0].buffer
        };
        const profileImageHash = await ipfsAddImage(profileImageUpload);
        req.body.profilePhotoIPFSHash = profileImageHash[0].hash;
      }
    }

    // Check Body Validation
    const { errors } = validateProfileInput(req.body);
    if (errors) {
      return res.status(400).json(error.details.map(detail => detail.message));
    }

    // Get Fields
    const profileFields = {};
    profileFields.user = req.user;
    if (req.body.website) profileFields.website = req.body.website;
    if (req.body.bio) profileFields.bio = req.body.bio;
    if (req.body.coverPhotoIPFSHash)
      profileFields.coverPhotoIPFSHash = req.body.coverPhotoIPFSHash;
    if (req.body.profilePhotoIPFSHash)
      profileFields.profilePhotoIPFSHash = req.body.profilePhotoIPFSHash;

    // Socials
    profileFields.social = {};
    if (req.body.youtube) profileFields.social.youtube = req.body.youtube;
    if (req.body.twitter) profileFields.social.twitter = req.body.twitter;
    if (req.body.facebook) profileFields.social.facebook = req.body.facebook;
    if (req.body.linkedin) profileFields.social.linkedin = req.body.linkedin;
    if (req.body.instagram) profileFields.social.instagram = req.body.instagram;

    // Update User profileImage
    let user = await User.findOne(req.user._id);
    if (user && req.body.profileImageHash) {
      await User.findOneAndUpdate(
        { _id: req.user._id },
        { $set: { profileImageHash: req.body.profileImageHash } },
        { new: true }
      ).then(user => {
        console.log(user);
      });
    }

    // Update Profile fulls
    const profile = await Profile.findOne({ "user._id": req.user._id });
    if (profile) {
      // Update
      // console.log('PROFILE EXISTS!', profile);
      await Profile.findOneAndUpdate(
        { "user._id": req.user._id },
        { $set: profileFields },
        { new: true }
      ).then(profile => {
        return res.json(profile);
      });
    } else {
      // Create new Profile
      console.log("NEW PROFILE!", profile);
      await new Profile(profileFields)
        .save()
        .then(profile => res.json(profile));
    }
  }
);

// @route     GET api/profile/user/:username
// @desc      Get profile by handle
// @access    Public
// TODO: FIX POPULATION OF USER
router.get("/username/:username", async (req, res) => {
  const errors = {};
  // Get Profile
  const profile = await Profile.findOne({
    "user.username": req.params.username
  });
  // .populate('user');

  if (!profile) {
    errors.noprofile = "There is No Profile for this user";
    return res.status(404).json(errors);
  }
  res.json(profile);
});

// @route     GET api/profile/user/:user_id
// @desc      Get profile by user_id
// @access    Public
router.get("/userid/:user_id", async (req, res) => {
  const errors = {};

  const profile = await Profile.findOne({ "user._id": req.params.user_id });
  if (!profile) {
    errors.noprofile = "There is No Profile for this user";
    return res.status(404).json(errors);
  }
  res.json(profile);
});

// @route   GET api/profile/all
// @desc    Get all profiles
// @access  Public
router.get("/all", async (req, res) => {
  const errors = {};

  const profiles = await Profile.find();

  if (!profiles) {
    errors.noprofile = "There are no profiles";
    return res.status(404).json(errors);
  }

  res.json(profiles);
});

/* NOT ACTIVE YET
// @route   DELETE api/profile
// @desc    Delete user and profile
// @access  Private
router.delete(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOneAndRemove({ user: req.user.id }).then(() => {
      User.findOneAndRemove({ _id: req.user.id }).then(() =>
        res.json({ success: true })
      );
    });
  }
);
*/

// Takes an object. each property is a file input with their own properties buffer, filename, and mimetype
const ipfsAddImage = async image => {
  const imageHash = await ipfs.add(image);
  return imageHash;
};
module.exports = router;
