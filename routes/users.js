/*
  Route: /api/users
*/
const { User, validate } = require("../models/user");
const { Profile } = require("../models/profile");

const _ = require("lodash");
const bcrypt = require("bcrypt");
const express = require("express");
const mongoose = require("mongoose");
const Joi = require("joi");
const router = express.Router();
// Fawn
const Fawn = require("fawn");

// errors
const errorTransformer = require("../utils/errorTransformer");

// Fawn.init(mongoose);

// Middleware
// const auth = require('../middleware/auth');
// const admin = require("../middleware/admin");
// const validateObjectId = require("../middleware/validateObjectId");
const passportAuth = require("../middleware/passportAuth");

// @route   GET /api/users
// @desc    Get tags
// @access  Public
router.get("/", async (req, res) => {
  const users = await User.find().sort("username");
  res.send(users);
});

// @route   POST /api/users/register
// @desc    Register a new user
// @access  Public
router.post("/register", async (req, res) => {
  // Validate Registration input. If errors return 400
  const { error } = validate(req.body);
  if (error) return res.status(400).json(errorTransformer(error));

  // Validate User's email doesn't already exist. Return 400 if exists
  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).json({ user: "User already registered." });
  // send;
  // Create new user object. Use lodash pick method to grab params from the body
  const newUser = new User(
    _.pick(req.body, ["username", "email", "password", "firstName", "lastName"])
  );

  // Hash and Salt the input password
  const salt = await bcrypt.genSalt(10);
  newUser.password = await bcrypt.hash(newUser.password, salt);

  // Create profile for user
  const newProfile = new Profile({
    user: newUser
  });

  // Fawn takes in a collection named in the db, then the object to be saved
  // Fawn is good for two-phase commits to the DB
  try {
    new Fawn.Task()
      .save("users", newUser)
      .save("profiles", newProfile)
      .run();
    res.send(_.pick(newUser, ["_id", "username", "email"]));
  } catch (ex) {
    console.log(ex);
    res.status(500).json({ server: "Something failed: " + ex.message });
  }
});

// @route   POST api/users/login
// @desc    Login User / Return JWT Token
// @access  Public
router.post("/login", async (req, res) => {
  // Validate Input
  const { errors } = validateLoginInput(req.body);
  if (errors) return res.status(404).json(errorTransformer(error));

  // Validate email exists
  let user = await User.findOne({ email: req.body.email.toLowerCase() });
  if (!user)
    return res.status(400).json({ login: "Invalid email or password." });

  // Validate password match
  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword)
    return res.status(400).json({ login: "Invalid email or password." });

  // Generate JWT token
  const token = await user.generateAuthToken();

  // Return Bearer JWT token in body and x-auth-token in header
  res.header("x-auth-token", token).json({
    success: true,
    token: `Bearer ${token}`
  });
});

// @route   GET /api/users/me
// @desc    Get current user
// @access  PRIVATE
router.get("/me", passportAuth, async (req, res) => {
  const user = await User.findById(req.user._id);
  if (!user)
    return res
      .status(404)
      .json({ notfound: "The user with the given ID was not found." });
  res.json(user);
});

// @route   DELETE /api/tracks/
// @desc    Deletes account
// @access  PRIVATE / AUTHOR ONLY
router.delete("/me", passportAuth, async (req, res) => {
  // Find user
  const user = await User.findById(req.user._id);
  if (!user)
    return res
      .status(404)
      .json({ notfound: "The user with the given ID was not found." });

  try {
    new Fawn.Task()
      .remove("users", { _id: user._id })
      .remove("profiles", { "user._id": user._id })
      .remove("tracks", { "owner._id": user._id })
      .run();
    res.status(200).send("Account Deleted");
  } catch (ex) {
    console.log(ex);
    res.status(500).json({ server: "Something failed: " + ex.message });
  }
});

function validateLoginInput(loginData) {
  let errors = {};
  const schema = {
    email: Joi.string()
      .min(5)
      .max(255)
      .email()
      .required(),
    password: Joi.string()
      .min(5)
      .max(255)
      .required()
  };
  return Joi.validate(loginData, schema, { abortEarly: false });
}
module.exports = router;
