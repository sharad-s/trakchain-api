/*
  Route: /api/tracks
*/
// Models
const { Tag } = require("../models/tag");
const { User } = require("../models/user");
const { Track, validate } = require("../models/track");

//imports
const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const ipfs = require("../utils/ipfs");

// errors
const errorTransformer = require("../utils/errorTransformer");

// Paypal
const paypal = require("paypal-rest-sdk");
paypal.configure({
  mode: "sandbox", //sandbox or live
  client_id:
    "ATSPjlDaImAqv73puqer3TmeXbXr-jSDAZfSLtVm4qJQQhpmjhPBFWe7gM-rURLn9SvURO4ut-5d8cgt",
  client_secret:
    "ENxdDSsh16b4HBKzbhWwzLRS_rP10JJlVSzFZqcZ--thF0waU8igAM6NNMFTCoORgH2agkZ1GovCN8Vx"
});

// Middleware
const passportAuth = require("../middleware/passportAuth");
// const admin = require("../middleware/admin");
const validateObjectId = require("../middleware/validateObjectId");
const {
  trackUpload,
  validateTrackUpload
} = require("../middleware/multiPartForms");

// @route   POST /api/tracks/test
// @desc    Post a file
// @access  Public
router.post("/test", trackUpload, validateTrackUpload, async (req, res) => {
  const { fileAudio, fileImage } = req.files;
  // const audioBuffer = new Buffer(req.files.fileAudio[0].buffer.data);
  const audio = {
    path: fileAudio[0].originalname,
    content: fileAudio[0].buffer
  };
  const image = {
    path: fileImage[0].originalname,
    content: fileImage[0].buffer
  };
  const hashes = await ipfsAddTrack(audio, image);
  res.send(hashes);
});

// @route   GET /api/tracks
// @desc    Get all tracks
// @access  Public
router.get("/", async (req, res) => {
  const tracks = await Track.find().sort("name");
  res.send(tracks);
});

// @route   GET /api/tracks/:id
// @desc    Get a track by id
// @access  Public
router.get("/:id", validateObjectId, async (req, res) => {
  const track = await Track.findById(req.params.id);
  if (!track) {
    return res
      .status(404)
      .json({ notfound: "The track with the given ID was not found." });
  }
  res.send(track);
});

// @route   GET /api/tracks/:tag
// @desc    Get all tracks with specified tag name
// @access  Public
router.get("/tag/:tagName", async (req, res) => {
  // console.log(req.params.tagName, typeof req.params.tagName);
  // Find tracks with matching tagName
  const tracks = await Track.find({
    "tags.name": req.params.tagName
  }).sort("name");

  // If tracks are empty, return 404
  if (!tracks || tracks.length === 0) {
    return res
      .status(404)
      .json({ tracks: "No tracks were found for the given tag." });
  }
  res.send(tracks);
});

// @route   GET /api/tracks/user/:userId
// @desc    Get all tracks belonging to specified user
// @access  Public
router.get("/user/:userId", async (req, res) => {
  // console.log(req.params.tagName, typeof req.params.tagName);
  // Find Tracks with matching owner Id
  const tracks = await Track.find({
    "owner._id": req.params.userId
  }).sort("name");

  // If tracks are empty, return 404
  if (!tracks || tracks.length === 0) {
    return res
      .status(404)
      .json({ tracks: "No tracks were found for the given user." });
  }

  res.send(tracks);
});

// @route   POST /api/tracks/
// @desc    Posts and saves a new tag
// @access  PRIVATE
router.post(
  "/",
  passportAuth,
  trackUpload,
  validateTrackUpload,
  async (req, res) => {
    console.log("1");
    // Extract fileAudio, fileIMage from req.files
    const { fileAudio, fileImage } = req.files;
    console.log("2");
    // Upload files to IPFS
    const audio = {
      path: fileAudio[0].originalname,
      content: fileAudio[0].buffer
    };
    console.log("3");
    const image = {
      path: fileImage[0].originalname,
      content: fileImage[0].buffer
    };
    console.log("4");
    const hashes = await ipfsAddTrack(audio, image);
    // console.log(hashes);
    console.log("5");
    req.body.ipfsHash = hashes.audio[0].hash;
    req.body.imageHash = hashes.image[0].hash;
    console.log("6");
    if (typeof req.body.tags === "string") {
      req.body.tags = req.body.tags.split(",");
    }
    //
    // Validate req.body. If validation fails, return 400 - Bad Request
    const { error } = validate(req.body);
    if (error) {
      console.log(error.details);
      return res.status(400).send(errorTransformer(error));
    }

    // Validate ownerId
    const owner = await User.findById(req.user._id);
    if (!owner) return res.status(400).send("Invalid Owner");

    // Validate and return array of tags.
    const tags = await checkAndSaveTags(req.body.tags);

    // Create new track object
    const track = new Track({
      name: req.body.name,
      owner: {
        _id: owner._id,
        name: owner.username
      },
      price: req.body.price,
      ipfsHash: req.body.ipfsHash,
      imageHash: req.body.imageHash,
      tags
    });

    // Save tag object to DB
    await track.save();
    console.log(track);
    res.send(track);
  }
);

// @route   PUT /api/tracks/
// @desc    Edits and saves an existing track. Author only allowed
// @access  PRIVATE / AUTHOR ONLY
router.put("/:id", passportAuth, async (req, res) => {
  // Validate req.body. If validation fails, return 400 - Bad Request
  const { error } = validate(req.body);
  if (error) {
    console.log(error);
    return res.status(400).send(errorTransformer(error));
  }
  // Find Track by id and update (Update first method) // TODO: Query First Method. Check for Author
  const track = await Track.findById(req.params.id);

  // If Track doesn't exist, return 404
  if (!track)
    return res.status(404).send("The track with the given ID was not found.");

  // If Track doesn't belong to user, return 404 /TODO: Which Error code?
  if (track.owner._id.toString() !== req.user._id.toString())
    return res.status(401).send("Unauthorized - Not the Owner of this track");

  // Validate and return array of tags.
  const tags = await checkAndSaveTags(req.body.tags);

  // Set new values
  track.set({
    name: req.body.name,
    ipfsHash: req.body.ipfsHash,
    imageHash: req.body.imageHash,
    price: req.body.price,
    tags
  });

  // Save and return track
  track.save();
  res.json(track);
});

// @route   DELETE /api/tracks/
// @desc    Deletes an existing track. Author only allowed //TODO: ADMIN ALLOWED
// @access  PRIVATE / AUTHOR ONLY
router.delete("/:id", passportAuth, validateObjectId, async (req, res) => {
  // Find Track by id and update (Update first method) // TODO: Query First Method. Check for Author
  const track = await Track.findById(req.params.id);

  // If Track doesn't exist, return 404
  if (!track)
    return res.status(404).send("The track with the given ID was not found.");

  // If Track doesn't belong to user, return 404 /TODO: Which Error code?
  if (track.owner._id.toString() !== req.user._id.toString())
    return res.status(401).send("Unauthorized - Not the Owner of this track");

  await Track.deleteOne({ _id: track._id });

  res.send(track);
});

// @route   POST /api/tracks/buy/:id
// @desc    Deletes an existing track. Author only allowed //TODO: ADMIN ALLOWED
// @access  PRIVATE / AUTHOR ONLY
router.post("/buy/:id", passportAuth, validateObjectId, async (req, res) => {
  // Find Track by id and update (Update first method) // TODO: Query First Method. Check for Author
  const track = await Track.findById(req.params.id);

  // If Track doesn't exist, return 404
  if (!track)
    return res.status(404).send("The track with the given ID was not found.");

  // If Track doesn't belong to user, return 404 /TODO: Which Error code?
  if (track.owner._id.toString() !== req.user._id.toString())
    return res.status(401).send("Unauthorized - Not the Owner of this track");

  var create_payment_json = {
    intent: "sale",
    payer: {
      payment_method: "paypal"
    },
    redirect_urls: {
      return_url: "http://google.com",
      cancel_url: "http://bing.com"
    },
    transactions: [
      {
        item_list: {
          items: [
            {
              name: track.name,
              sku: "item",
              price: track.price,
              currency: "USD",
              quantity: 1
            }
          ]
        },
        amount: {
          currency: "USD",
          total: track.price
        },
        description: "Lease agreement for track"
      }
    ]
  };

  paypal.payment.create(create_payment_json, function(error, payment) {
    if (error) {
      throw error;
    } else {
      console.log("Create Payment Response");
      console.log(payment);
    }
  });

  res.send(track);
});

const checkAndSaveTags = tags => {
  return Promise.all(
    tags.map(async tagName => {
      // If tag does not exist yet, save it
      let tag = await Tag.findOne({ name: tagName });
      if (!tag) {
        tag = new Tag({ name: tagName });
        await tag.save();
      }
      return tag;
    })
  );
};

// Takes an object. each property is a file input with their own properties buffer, filename, and mimetype
const ipfsAddTrack = async (audio, image) => {
  console.log("ipfs 1");
  const audioHash = await ipfs.add(audio);
  const imageHash = await ipfs.add(image);
  console.log("ipfs 2");
  // await ipfs.pin.add(audioHash);
  // await ipfs.pin.add(imageHash);
  console.log("ipfs 3");
  const ipfsHashes = {
    audio: audioHash,
    image: imageHash
  };
  console.log("ipfs 4");
  return ipfsHashes;
};

module.exports = router;
