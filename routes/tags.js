/*
  Route: /api/tags
*/
const { Tag, validate } = require("../models/tag");

const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();

// Middleware
// const auth = require("../middleware/auth");
// const admin = require("../middleware/admin");
// const validateObjectId = require("../middleware/validateObjectId");
const passportAuth = require("../middleware/passportAuth");

// @route   GET /api/tags
// @desc    Get tags
// @access  Public
router.get("/", async (req, res) => {
  const tags = await Tag.find()
    .sort("name")
    .limit(7);
  res.send(tags);
});

// @route   POST /api/tags/
// @desc    Posts and saves a new tag
// @access  PRIVATE
router.post("/", passportAuth, async (req, res) => {
  // Validate tag
  // If validation fails, return 400 - Bad Request
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Ensure tag is unique
  let tag = await Tag.findOne({ name: req.body.name });
  if (tag) return res.status(400).send("Error: Tag already exists.");

  // Create new tag object
  tag = new Tag({ name: req.body.name });

  // Save tag object to DB
  await tag.save();
  res.send(tag);
});

/*
// @route   GET /api/tags/:id
// @desc    Tests post route
// @access  Public
router.get("/:id", validateObjectId, async (req, res) => {
  // Find genre by id
  const genre = await Genre.findById(req.params.id);
  if (!genre) {
    return res.status(404).send("The genre with the given ID was not found.");
  }
  res.send(genre);
});



// PUT update existing course
router.put("/:id", async (req, res) => {
  // Validate course. If fail - return 400 - Bad Request
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Find Course by id and update (Update first method)
  const genre = await Genre.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name
    },
    { new: true }
  );

  // If not existing, return 404
  if (!genre)
    return res.status(404).send("The genre with the given ID was not found.");

  res.send(genre);
});

router.delete("/:id", [auth, admin], async (req, res) => {
  const genre = await Genre.findByIdAndRemove(req.params.id);

  // If course not found - return return 404
  if (!genre)
    return res.status(404).send("The genre with the given ID was not found.");

  res.send(genre);
});
*/
module.exports = router;
