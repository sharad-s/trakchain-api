const mongoose = require("mongoose");
const { trackSchema } = require("./track");
const Joi = require("joi");

const userSchemaSubset = new mongoose.Schema({
	username: {
		type: String,
		required: true,
		minlength: 1,
		maxlength: 50,
		trim: true
	},
	email: {
		type: String,
		required: true,
		minlength: 1,
		maxlength: 255,
		lowercase: true,
		trim: true
	},
	firstName: {
		type: String,
		minlength: 1,
		maxlength: 50,
		trim: true
	},
	lastName: {
		type: String,
		minlength: 1,
		maxlength: 50,
		trim: true
	},
	role: {
		type: String,
		enum: ["PRODUCER", "CUSTOMER"]
	}
});

// Create Schema
const transactionSchema = new mongoose.Schema({
	vendor: {
		type: userSchemaSubset,
		required: true
	},
	customer: {
		type: userSchemaSubset,
		required: true
	},
	track: {
		type: trackSchema,
		required: true
	},
	leaseType: {
		type: String,
		default: "DIRECT"
		// required: true
	},
	price: {
		type: Number,
		required: true
	}
});

const Transaction = mongoose.model("Transaction", transactionSchema);

function validateTransaction(transaction) {
	// Validate Course
	const schema = {
		vendorId: Joi.objectId().required(),
		customerId: Joi.objectId().required(),
		trackId: Joi.objectId().required()
	};
	return Joi.validate(transaction, schema, { abortEarly: false });
}

module.exports = {
	transactionSchema,
	Transaction,
	validateTransaction
};
