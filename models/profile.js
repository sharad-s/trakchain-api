const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Joi = require("joi");

// Create Schema
const profileSchema = new Schema({
  user: new mongoose.Schema({
    username: {
      type: String,
      required: true,
      minlength: 2,
      maxlength: 50,
      trim: true,
      required: true
    }
  }),
  // handle: {
  //   type: String,
  //   required: true,
  //   max: 40
  // },
  website: {
    type: String,
    trim: true
  },
  coverPhotoIPFSHash: {
    type: String,
    default: "QmfLsJW2UiSfecopgfNBVfiwmkzzYYAYovmM3HbTJm68zp"
  },
  profilePhotoIPFSHash: {
    type: String,
    default: "QmaNi8jKogPRnpNGybqJG3apn34qisDp5JQnkttfJX1hPm"
  },
  location: {
    type: String
  },
  bio: {
    type: String,
    default: "",
    trim: true
  },
  social: {
    youtube: {
      type: String,
      trim: true
    },
    twitter: {
      type: String,
      trim: true
    },
    facebook: {
      type: String,
      trim: true
    },
    linkedin: {
      type: String,
      trim: true
    },
    instagram: {
      type: String,
      trim: true
    }
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const Profile = mongoose.model("profile", profileSchema);

// Joi returns either result or error
function validateProfileInput(profile) {
  // Validate Course
  const schema = {
    bio: Joi.string(),
    website: Joi.string(),
    coverPhotoIPFSHash: Joi.string(),
    profilePhotoIPFSHash: Joi.string()
  };
  return Joi.validate(profile, schema, { abortEarly: false });
}

module.exports = {
  Profile,
  profileSchema,
  validateProfileInput
};
