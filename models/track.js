const mongoose = require("mongoose");
const Joi = require("joi");
const { tagSchema } = require("./tag");

// Create Schema
const trackSchema = new mongoose.Schema({
  name: {
    type: String,
    minlength: 1,
    maxlength: 30,
    required: true
  },
  owner: new mongoose.Schema({
    name: {
      type: String,
      required: true,
      minlength: 2,
      maxlength: 50,
      trim: true
    }
  }),
  ipfsHash: {
    type: String,
    required: true
  },
  imageHash: {
    type: String
  },
  price: {
    type: Number,
    required: true
  },
  tags: {
    type: [tagSchema],
    required: true,
    unique: true
  }
});

const Track = mongoose.model("Track", trackSchema);

// Joi returns either result or error
function validateTrack(track) {
  // Validate Course
  const schema = {
    name: Joi.string()
      .min(1)
      .max(30)
      .required(),
    ipfsHash: Joi.string().required(),
    imageHash: Joi.string().required(),
    price: Joi.number().required(),
    tags: Joi.array()
      .items(Joi.string())
      .min(1)
      .required()
  };
  return Joi.validate(track, schema, { abortEarly: false });
}

module.exports = {
  Track,
  trackSchema,
  validate: validateTrack
};
