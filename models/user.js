const mongoose = require('mongoose');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const config = require('config');

// Create Schema
const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 50,
    trim: true
  },
  email: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 255,
    lowercase: true,
    unique: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 1024
  },
  firstName: {
    type: String,
    minlength: 1,
    maxlength: 50,
    trim: true
  },
  lastName: {
    type: String,
    minlength: 1,
    maxlength: 50,
    trim: true
  },
  profileImageHash: {
    type: String,
    default: ''
  },
  role: {
    type: String,
    enum: ['PRODUCER', 'CUSTOMER'],
    default: 'CUSTOMER'
  }
});

userSchema.methods.generateAuthToken = function() {
  // Generate payload, options
  const payload = { _id: this._id, name: this.name };
  const opts = { expiresIn: 3600 };

  // Sign JWT payload with secret key and send JWT Bearer Token //Why bearer token?
  const token = jwt.sign(payload, config.get('jwtPrivateKey'), opts);
  return token;
};

const User = mongoose.model('User', userSchema);

// Joi returns either result or error
function validateUser(user) {
  // Validate Course
  const schema = {
    username: Joi.string()
      .min(1)
      .max(50)
      .required(),
    email: Joi.string()
      .min(1)
      .max(255)
      .email()
      .required(),
    password: Joi.string()
      .min(5)
      .max(255)
      .required(),
    firstName: Joi.string()
      .min(1)
      .max(50)
      .required(),
    lastName: Joi.string()
      .min(1)
      .max(50)
      .required(),
    profileImageHash: Joi.string(),
    role: Joi.string().valid('PRODUCER', 'CUSTOMER')
  };
  return Joi.validate(user, schema, { abortEarly: false });
}

module.exports = {
  User,
  userSchema,
  validate: validateUser
};
