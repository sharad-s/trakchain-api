const mongoose = require("mongoose");
const Joi = require("joi");

// Schema can be re-used in multiple place
const tagSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 15,
    lowercase: true,
    trim: true
    // unique: true
  }
});

// Instantiate Genre class
const Tag = mongoose.model("Tag", tagSchema);

// Joi returns either result or error
function validateTag(tag) {
  // Validate Course
  const schema = {
    name: Joi.string()
      .min(3)
      .max(15)
      .required()
  };
  return Joi.validate(tag, schema);
}

module.exports = {
  Tag,
  tagSchema,
  validate: validateTag
};
