// Default middleware initialized at startup

const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const winston = require('winston');
const passport = require('passport');

// Debuggers
const startupDebugger = require('debug')('app:startup');

module.exports = function(app) {
  // Body Parser Middleware
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  // Protects common HTML security vulnerabilities
  app.use(helmet());

  // Passport auth middleware & config
  app.use(passport.initialize());
  require('../config/passport')(passport);

  // Development Middleware
  if (app.get('env') === 'development') {
    // Logs API requests
    app.use(morgan('tiny'));
    // startupDebugger("Morgan enabled... Requests will be logged.");
    winston.info('Morgan enabled... Requests will be logged.');
  }
};
