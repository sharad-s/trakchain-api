const mongoose = require("mongoose");
const winston = require("winston");
const config = require("config");
const Fawn = require("fawn");

module.exports = function() {
	// Connect to MongoDB
	const db = config.get("db");
	mongoose.connect(db).then(() => winston.info(`Connected to ${db}...`));

	// Init Fawn
	Fawn.init(mongoose);
};
