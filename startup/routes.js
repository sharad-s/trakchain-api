const express = require("express");
// const error = require("../middleware/error");

// Import Routes
const home = require("../routes/home");
const tags = require("../routes/tags");
const tracks = require("../routes/tracks");
const users = require("../routes/users");
const profiles = require("../routes/profiles");
const transactions = require("../routes/transactions");

module.exports = function(app) {
	// Setup Routes
	app.use("/", home);
	app.use("/api/tags", tags);
	app.use("/api/tracks", tracks);
	app.use("/api/users", users);
	app.use("/api/profiles", profiles);
	app.use("/api/transactions", transactions);

	// app.use(error);
};
