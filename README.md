# Trakchain API

## Get started

* Install Dependencies: `npm install` or `yarn`

## Testing

* Run mongo daemon in a shell: `mongod`

* Change NODE_ENV to test: `export NODE_ENV=test`

* Run tests: `npm run test`

## Task List

### User Login / Authentication

* [x] User model and Joi schema
* [x] User route / Auth route
* [x] Passport Authentication
* [x] Authorized Routes x
* [x] www-url-form-encoded

### Full track functionality

* [x] Tracks can be edited/deleted.
* [x] Tracks can be played on the client (client)
* [x] Get tracks by tag(s)
* [x] Get tracks by userId
* [ ] Track upload to IPFS (on the backend)
* [ ] Multipart-form data for file upload ... in progress

### Security

* [ ] Errors are returned as json object and capable of being picked up on (might need to write middleware)
* [ ] Tracks are encrypted and/or watermarked for public facing use.

### User Profiles

* [ ] Profile Model ...
* [ ] Profile Route ...
* [ ] IPFS Profile videos
* [ ] Redux actions (client side)
