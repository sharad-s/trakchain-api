const ipfsAPI = require("ipfs-api");

// using the infura.io node
// const ipfs = ipfsAPI({
//   host: 'ipfs.infura.io',
//   port: 5001,
//   protocol: 'https'
// });

const ec2IpfsIp = "18.223.115.149";
const ec2IpfsPort = "5001";

// //run with local daemon
// const ipfs = ipfsAPI('localhost', '5001', { protocol: 'http' });

// run with ec2
const ipfs = ipfsAPI(ec2IpfsIp, ec2IpfsPort, { protocol: "http" });

module.exports = ipfs;

// const IPFS = require('ipfs-mini');
// const isString = require('lodash/fp/isString');
//
// const ipfs = new IPFS({
//   host: 'ipfs.infura.io',
//   port: 5001,
//   protocol: 'https'
// });
//
// async function ipfsGetData(multihash) {
//   if (!isString(multihash)) {
//     return new Error('multihash must be String');
//   } else if (!multihash.startsWith('Qm')) {
//     return new Error('multihash must start with "Qm"');
//   }
//
//   return new Promise((resolve, reject) => {
//     ipfs.catJSON(multihash, (err, result) => {
//       if (err) reject(new Error(err));
//       resolve(result);
//     });
//   });
// }
//
// async function ipfsAddObject(obj) {
//   const CID = await new Promise((resolve, reject) => {
//     ipfs.addJSON(obj, (err, result) => {
//       if (err) reject(new Error(err));
//       resolve(result);
//     });
//   });
//   console.log('CID:', CID);
//   return CID;
// }
//
// module.exports = {
//   ipfsAddObject,
//   ipfsGetData
// };
